local S = axinitium.intllib

--Register Axinite Pickaxe
minetest.register_tool("axinitium:pickaxe", {
	description = S("Axinite Pickaxe"),
	inventory_image = "pickaxe.png",
	range = 8,
	tool_capabilities = {
		full_punch_interval = 0.9,
		max_drop_level=3,
		groupcaps={
			cracky = {times={[1]=2.0, [2]=1.0, [3]=0.50}, uses=50, maxlevel=3},
		},
		damage_groups = {fleshy=5},
	},
})

--Register Axinite Axe
minetest.register_tool("axinitium:axe", {
	description = S("Axinite Axe"),
	inventory_image = "axe.png",
	range = 8,
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			choppy={times={[1]=2.10, [2]=0.90, [3]=0.50}, uses=30, maxlevel=3},
		},
		damage_groups = {fleshy=7},
	}
})

--Register Axinite shovel
minetest.register_tool("axinitium:shovel", {
	description = S("Axinite Shovel"),
	inventory_image = "shovel.png",
	wield_image = "shovel.png^[transformR90",
	range = 8,
	tool_capabilities = {
		full_punch_interval = 0.8,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=1.10, [2]=0.50, [3]=0.30}, uses=50, maxlevel=3},
		},
		damage_groups = {fleshy=4},
	},
})

--Register Axinite sword
minetest.register_tool("axinitium:sword", {
	description = S("Axinite Sword"),
	inventory_image = "sword.png",
	range = 8,
	tool_capabilities = {
		full_punch_interval = 0.5,
		max_drop_level=3,
		groupcaps={
			snappy={times={[1]=1.90, [2]=0.90, [3]=0.30}, uses=150, maxlevel=3},
		},
		damage_groups = {fleshy=10},
	}
})

minetest.register_tool("axinitium:crystaline_bell", {
	description = S("Crystaline Bell"),
	inventory_image = "crystalline_bell.png",
	on_use = function(itemstack, user, pointed_thing)
		if pointed_thing.type ~= "node" then
			return
		end
		local pos = pointed_thing.under
		local node = minetest.get_node(pos)
		local growth_stage = 0
		if node.name == "axinitium:crystal_ore4" then
			growth_stage = 4
		elseif node.name == "axinitium:crystal_ore3" then
			growth_stage = 3
		elseif node.name == "axinitium:crystal_ore2" then
			growth_stage = 2
		elseif node.name == "axinitium:crystal_ore" then
			growth_stage = 1
		end
		if growth_stage == 4 then
			node.name = "axinitium:crystal_ore3"
			minetest.swap_node(pos, node)
		elseif growth_stage == 3 then
			node.name = "axinitium:crystal_ore2"
			minetest.swap_node(pos, node)
		elseif growth_stage == 2 then
			node.name = "axinitium:crystal_ore1"
			minetest.swap_node(pos, node)
		end
		if growth_stage > 1 then
			itemstack:add_wear(65535 / 100)
			local player_inv = user:get_inventory()
			local stack = ItemStack("axinitium:ore_lingot")
			if player_inv:room_for_item("main", stack) then
				player_inv:add_item("main", stack)
			end
			return itemstack
		end
	end,
})

 --can take dirt with grass (ethereal, tenplus1)

local old_handle_node_drops = minetest.handle_node_drops

function minetest.handle_node_drops(pos, drops, digger)

	-- are we holding Axinite Plus Shovel?
	if digger:get_wielded_item():get_name() ~= "axinitium:shovel_plus" then
		return old_handle_node_drops(pos, drops, digger)
	end

	local nn = minetest.get_node(pos).name

	if minetest.get_item_group(nn, "crumbly") == 0 then
		return old_handle_node_drops(pos, drops, digger)
	end

	return old_handle_node_drops(pos, {ItemStack(nn)}, digger)
end

minetest.register_tool("axinitium:shovel_plus", {
	description = S("Shovel plus"),
	inventory_image = "shovel_plus.png",
	range = 8,
	tool_capabilities = {
		full_punch_interval = 1.0,
		max_drop_level=1,
		groupcaps={
			crumbly = {times={[1]=2.10, [2]=1.50, [3]=1.30}, uses=5, maxlevel=3},
		},
		damage_groups = {fleshy=4},
	},
	sound = {breaks = "default_tool_breaks"},
})
