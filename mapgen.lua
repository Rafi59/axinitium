-- Axinite Biome

axinitium.mg = {}
axinitium.mg.path = minetest.get_modpath("axinitium")

-- Axinite Biome Mountains
minetest.register_biome({
    name             = "axinite_mountain",
    node_top         = "axinitium:dirt",
    depth_top        = 1,
    node_filler      = "default:stone",
    depth_filler     = 3,
	y_min       = 85,
	y_max       = 31000,
	heat_point       = 80.0,
	humidity_point   = 15.0,
})

-- Axinite Biome Lowland
minetest.register_biome({
    name             = "axinite_lowland",
    node_top         = "axinitium:dirt",
    depth_top        = 1,
    node_filler      = "default:dirt",
    depth_filler     = 3,
	y_min       = 1,
	y_max       = 5,
	heat_point       = 10.0, --45.0
	humidity_point   = 40.0, --55.0
})

-- Register Biome Decoration
local path = minetest.get_modpath("axinitium")

minetest.register_decoration({
    deco_type = "simple",
    place_on = "axinitium:dirt",
    sidelen = 16,
    fill_ratio = 0.0008,
    biomes = {"axinite_mountain","axinite_lowland"},-- Spawn in both biomes
    decoration = "axinitium:crystal_ore1",
--  height = 2,
    height_max = 1,
})

-- axinitium tree, spawn in Axinite Mountains Biomes
minetest.register_decoration({
    deco_type = "schematic",
    place_on = "axinitium:dirt",
    sidelen = 16,
    fill_ratio = 0.01,
    biomes = {"axinite_mountain"},
    schematic = path.."/schematics/tree.mts",
    flags = "place_center_x, place_center_z",
    rotation = "random",
})

-- Small Lava Crater -- from ethereal lava pits, spawn in Lowland Biomes
minetest.register_decoration({
    deco_type = "schematic",
    place_on = "axinitium:dirt",
    sidelen = 16,
    fill_ratio = 0.002,
    biomes = {"axinite_lowland"},
    schematic = path.."/schematics/volcan.mts",
    flags = "place_center_x, place_center_z",
    rotation = "random",
})
